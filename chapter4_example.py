# -*- coding: utf-8 -*-
"""
Created on Mon May  4 16:35:45 2015

@author: assumpta
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as py
from numpy import linalg as LA
from math import gamma
from my_cheb_code import cheb

py.figure('1')
'''Steady state fractional advection equation where there is exponential convergence.'''
n=30
[D,x] = cheb(n,0.5)
nu = 0.5
f = gamma(7+ 9./17)/gamma(7+ 9./17 - nu) * (1+x)**(6 + 9./17 - nu)
f= f.reshape(n-1,1)
U=np.linalg.solve(D,f) 

u = ((1+x)**(6 + 9./17)).reshape(n-1,1)  # exact solution

py.plot(x,u,'-',markersize=8,label='Exact')
py.plot(x,U,'ro',markersize=8,label='Approximate')
py.legend(loc=2)
py.title('Max Error = '+str('%.4e' % LA.norm(U-u,np.inf)),fontsize=15)
py.xlabel('$x$',fontsize=17)
py.ylabel('$u(x)$',fontsize=17)

py.figure('2')
''' Convergence plot  '''
def expcon(n):
    ''' A function that takes 'n', and returns the L infinity norm error  '''
    nu = 0.5
    [D,x] = cheb(n,0.5)
    f = gamma(7+ 9./17)/gamma(7+ 9./17 - nu) * (1+x)**(6 + 9./17 - nu)
    f= f.reshape(n-1,1)
    u = ((1+x)**(6 + 9./17)).reshape(n-1,1)  # Exact solution
    U=np.linalg.solve(D,f)                   # Approximated solution
    error = LA.norm((U-u),np.inf)
    return error

n1 = np.arange(3,25)
Ep = [expcon(i) for i in n1]
py.semilogy(n1,Ep,'ro-',label='Error')
G = np.exp(-6.5*n1**(1/2))                  # Subgeometric convergence  
py.semilogy(n1,G,'gd',markersize=6,label=' $\mathbf{e^{-6.5\sqrt{n}}}$')
py.xlabel('n'); py.ylabel('$L^{\infty}\;$error')
py.xlim(0,25)
py.ylim(10**(-14),100)
py.xticks(np.arange(0,26, 5))
py.title('Log-Linear plot')
py.legend()
py.show()














