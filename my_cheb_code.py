# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:58:28 2015

@author: chinenye
"""

from __future__ import division
import numpy as np
from math import gamma
from numpy.linalg import inv

'''Functions to be used for the differentiation matrix code.'''
def V_leg(n):
    '''
    A function that takes 'n'and returns the (n+1)by(n+1)
    Vandermonde-Legendre matrix.
    '''
    x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points
    A = np.outer(x,x)
    A[0] = np.ones((n+1))                                  # First legendre polynomial 
    A[1] = x                                               # Second legendre polynomial
    for k in range(1,n):                                   # For other recursive terms 
        for i in range(n+1):
            A[k+1,i] = ((2*k+1)*A[1,i]*A[k,i] - k*A[k-1,i])/(k+1)       
    return A.transpose()                                   # The matrix.


def V_jac(n,mu):
    '''
    A function that takes 'n' and 'mu' (order), computes the Vandermonde's
    matrix of Jacobi polynomial and returns an (n+1)by(n+1) matrix.
    '''
    x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points 
    A = np.outer(x,x)
    a = -mu
    b = mu
    A0 = 0.5*(a+b)+1
    B0 = 0.5*(a-b)
    A[0] = np.ones((n+1))                                  # First Jacobi polynomial 
    A[1] = A0*x + B0                                       # Second Jacobi polynomial
    for k in range(1,n):                                   # For other recursive terms
        for i in range(n+1):
            An =((2*k+a+b+1)*(2*k+a+b+2))/(2*(k+1)*(k+a+b+1))
            Bn = ((a**2-b**2)*(2*k+a+b+1))/(2*(k+1)*(k+a+b+1)*(2*k+a+b))
            Cn = ((k+a)*(k+b)*(2*k+a+b+2))/((k+1)*(k+a+b+1)*(2*k+a+b))
            A[k+1,i] = (An*x[i] + Bn)*A[k,i] - Cn*A[k-1,i]
    return A.transpose()                                   # The matrix

''' The differentiation matrix code  '''

def cheb(n,mu):
    '''
    A function that takes 'n' and 'mu' (the differentiation order), computes the 
    Chebyshev differntiation matrix and returns:
    i.  (n+1)by(n+1) matrix for ordinary differentiation and the Chebyshev points 
    ii. (n-1)by(n-1)matrix for fractional differentiation and the Chebyshev points.
    Here, the Chebyshev points (x) goes from -1 to 1.
    '''
    if mu == 1:                                           
        ''' comuputes for the ordinary differentiation matrix '''
        if n == 0:
            print "n should be greater than zero"
        else:
            k= np.linspace(0/n,n/n,n+1)    
            D=np.outer(k,k)
            D[0][0]= -(2* n**2 +1)/6
            D[n][n]=(2* n**2 +1)/6
            D[n][0] = (-1)**n /2
            D[0][n] = -(-1)**n /2
            for j in range(n+1):
               for l in range(n+1):
                    if j!= l:
                        if k[j]==0 or k[j]==n:
                            c = 2
                        else:
                            c =1
                            D[j][l] = -c *(-1)**(j+l)/float((c)*(((np.cos(k[j]*np.pi))-(np.cos(k[l]*np.pi)))))
            for i in range(1,n):
                D[i][i] = -(-1)* np.cos(k[i]*np.pi)/float(2*(1-(np.cos(k[i]*np.pi))**2))
                D[0][i] = -2*(-1)**(i)/(1 - (np.cos(k[i]*np.pi)))
                D[n][i] = 2*(-1)**(i+n)/(1 + (np.cos(k[i]*np.pi)))
                D[i][0] = 1*(-1)**(i)/(2*(1 - (np.cos(k[i]*np.pi))))
                D[i][n] = -(-1)**(i+n)/(2*(1 + (np.cos(k[i]*np.pi))))
            x = np.array([-np.cos(np.pi*j/(n)) for j in range(n+1)])
            return D,x                                     # The matrix D and the grid points.
    else:
        ''' Computes for fractional differentiation matrix  '''
        k = np.zeros(n)
        A = np.outer(k,k)                                  # Initial declaration of matrix to store values into it                             
        A1 = np.outer(k,k)                                 # Initial declaration of matrix to store values into it   
        B = []
        x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
        B1 = 1/((x+1)**mu)
        for k in range(1,n+1):          
            B.append(gamma(k+mu)/gamma(k))
            
        for i in range(n):
            for j in range(n):
                if i == j:
                    A[i,j] = B[i]
                    A1[i,j] = B1[i]
    
        A = np.mat(A)                                      # Matrix for 1/(x+1)^mu
        A1 = np.mat(A1)                                    # Matrix for gamma(n+mu)/gamma(n)
        P=np.mat(V_leg(n-1))                               # The legendre polynomial
        j1=inv(np.matrix(V_jac(n-1,mu)))                   # Inverse of the Jacobi polynomial
        D=P*A*j1*A1
           
        D=D[1:n,1:n]                                       # Boundary conditions
        x = x[1:]
        return  np.array(D),x                              # The matrix D and the grid points.  
            
        