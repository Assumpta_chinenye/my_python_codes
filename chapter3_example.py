# -*- coding: utf-8 -*-
"""
Created on Fri May  1 14:19:21 2015

@author: Chinenye Assumpta Nnakenyi
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as py
from numpy import linalg as LA
from my_cheb_code import cheb

py.figure('1')
n=50
x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)]) # Chebyshev points
D = cheb(n,1)[0]
Dd = np.dot(D,D) + np.eye(n+1)
Dd = Dd[1:-1,1:-1]
f = np.exp(-2*x[1:-1])           
u = np.linalg.solve(Dd,f)
u = np.hstack(([0.],u,[0.]))                          # Approximate solution

c1 = -np.cosh(2)/(5*np.cos(1))
c2 = np.sinh(2)/(5*np.sin(1))
exact = c1*np.cos(x) + c2*np.sin(x) + np.exp(-2*x)/5   # Exact solution

py.plot(x,exact,'-',markersize=8, label='Exact')
py.plot(x,u,'ro',markersize=8, label='Approximate')
py.xlabel('$\mathbf{x}$',fontsize=27)
py.ylabel('$\mathbf{u(x)}$',fontsize=27)
py.title('max err = '+str('%.3e' % LA.norm(u-exact,np.inf)),fontsize=25)
py.legend(loc=4,fontsize=17)
py.show()

''' Convergence code and plot  '''
py.figure('2')
def err(n):
    x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
    D = cheb(n,1)[0]
    Dd = np.dot(D,D) + np.eye(n+1)
    Dd = Dd[1:-1,1:-1]
    f = np.exp(-2*x[1:-1])           
    u = np.linalg.solve(Dd,f)            
    u = np.hstack(([0.],u,[0.]))
    c1 = -np.cosh(2)/(5*np.cos(1))
    c2 = np.sinh(2)/(5*np.sin(1))
    exact = c1*np.cos(x) + c2*np.sin(x) + np.exp(-2*x)/5
    error = LA.norm(u-exact,np.inf)
    return error
    
NN = np.arange(2,50,1)
Ep = [err(i) for i in NN]
G = np.exp(-3.0*NN)                                    # Geometric convergence
py.semilogy(NN,G,'*',markersize=8,label='$\mathbf{e^{-3n}}$')
py.semilogy(NN,Ep,'o-',markersize=8,label='Error')
py.ylim(10**(-15), 10**(-2)) 
py.xlabel('$\mathbf{n}$',fontsize=27)
py.ylabel('$\mathbf{L^\infty}$ error',fontsize=27)
py.legend(fontsize=17)
py.title('Log-Linear',fontsize=25)




