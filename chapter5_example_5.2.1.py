# -*- coding: utf-8 -*-
"""
Created on Fri May 15 15:00:00 2015

@author: assumpta
"""

from __future__ import division
import matplotlib.pyplot as py
import numpy as np
from my_cheb_code import cheb

''' Dampling oscillation for D '''
n=40
D = cheb(n,1)[0]
A = 1
B = 2
C = 1
L = A*np.dot(D,D) + B*D + C * np.identity(n+1)
L[0]= np.hstack(([1],np.zeros(n))) # boundary conditions
L[1] = D[0]                        # boundary conditions  
x = np.array([-np.cos(np.pi*j/(n)) for j in range(n+1)])
c = (np.sin(np.pi*4*x)).reshape(n+1,1)
c[0] = 0
c[1] = 0
Y=np.linalg.solve(L,c)            # Approximate solutions
a = np.exp(-x-1)*(4*np.pi*(x+16*np.pi**2*(x+1)+3) - (16*np.pi**2 - 1)*np.exp(x+1)*np.sin(4*np.pi*x)- 8*np.pi*np.exp(x+1)*np.cos(4*np.pi*x) )
b = (1+16*np.pi**2)**2
exact = a/b                       # Exact solutions
py.plot(x,exact,'-',label='Exact')
py.plot(x,Y,'ro',markersize=8,label='Approximate')
py.xlabel('$\mathbf{x}$',fontsize=27)
py.ylabel('$\mathbf{y(x)}$',fontsize=27)
py.legend(loc=4,fontsize=17)

py.figure('2')
def erro(n):
    D = cheb(n-1,1)[0]
    A = 1
    B = 2
    C = 1
    L = A*np.dot(D,D) + B*D + C * np.identity(n)
    L[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
    L[1] = D[0]                        # boundary conditions  
    x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
    c = (np.sin(np.pi*4*x)).reshape(n,1)
    c[0] = 0
    c[1] = 0
    Y=np.linalg.solve(L,c)
    a = np.exp(-x-1)*(4*np.pi*(x+16*np.pi**2*(x+1)+3) - (16*np.pi**2 - 1)*np.exp(x+1)*np.sin(4*np.pi*x)- 8*np.pi*np.exp(x+1)*np.cos(4*np.pi*x) )
    b = (1+16*np.pi**2)**2
    exact = (a/float(b)).reshape(n,1)
    error = np.linalg.norm(Y-exact,np.inf)
    return error
   
NN = np.arange(10,40,2)
EPS = [erro(i) for i in NN]
py.semilogy(NN,EPS,'-o',markersize=8,label='Error')
py.semilogy(NN,np.exp(-1.25*NN)*10**7,'*',markersize=8,label='$\mathcal{O}[e^{-1.25n}]$')
py.xlabel('$\mathbf{n}$',fontsize=27)
py.ylabel('$\mathbf{L^\infty}$ error',fontsize=27)
py.title('Log-linear',fontsize=25)
py.legend(fontsize=17)
py.ylim(10**(-13),10)
py.show()