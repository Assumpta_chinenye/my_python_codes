# -*- coding: utf-8 -*-
"""
Created on Fri May 15 15:12:32 2015

@author: assumpta
"""

from __future__ import division
import matplotlib.pyplot as py
import numpy as np
from my_cheb_code import cheb

''' Comparing D, D^{0.5} and D^{1.5}'''
n=100
D = cheb(n-1,1)[0]
Df = cheb(n+1,0.5)[0]
Df2 = cheb(n+1,1.5)[0]
A = 1
B = 2
C = 1

''' For D '''
L = A*np.dot(D,D) + B*D + C * np.identity(n)
L[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
L[1] = D[0]                        # boundary conditions  
x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
t = (x+1)/2
c = (np.sin(np.pi*4*x)).reshape(n,1)
c[0] = 0
c[1] = 0
Y=np.linalg.solve(L,c)
py.plot(x,Y,'-*',markersize = 8,label='$D^{1}$')

''' For D^{0.5} '''
LL = A*np.dot(D,D) + B*Df + C * np.identity(n)
LL[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
LL[1] = D[0]                        # boundary conditions  
x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
t = (x+1)/2
c = (np.sin(np.pi*4*x)).reshape(n,1)
c[0] = 0
c[1] = 0
YY=np.linalg.solve(LL,c)
py.plot(x,YY,'-o',markersize = 8,label='$D^{0.5}$')

''' For D^{1.5} '''
LL2 = A*np.dot(D,D) + B*Df2 + C * np.identity(n)
LL2[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
LL2[1] = D[0]                        # boundary conditions  
x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
t = (x+1)/2
c = (np.sin(np.pi*4*x)).reshape(n,1)
c[0] = 0
c[1] = 0
YY2=np.linalg.solve(LL2,c)
py.plot(x,YY2,'-^',markersize = 8, label='$D^{1.5}$')
py.legend(loc=2)
py.xlabel('$x$',fontsize=27)
py.ylabel('$y(x)$',fontsize=27)
py.show()

''' Log-linear plots for D1, D0.5 and D1.5 '''
# For D1
py.figure('2')
def erro(n):
    D = cheb(n-1,1)[0]
    A = 1
    B = 2
    C = 1
    L = A*np.dot(D,D) + B*D + C * np.identity(n)
    L[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
    L[1] = D[0]                        # boundary conditions  
    x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
    c = (np.sin(np.pi*4*x)).reshape(n,1)
    c[0] = 0
    c[1] = 0
    Y=np.linalg.solve(L,c)
    a = np.exp(-x-1)*(4*np.pi*(x+16*np.pi**2*(x+1)+3) - (16*np.pi**2 - 1)*np.exp(x+1)*np.sin(4*np.pi*x)- 8*np.pi*np.exp(x+1)*np.cos(4*np.pi*x) )
    b = (1+16*np.pi**2)**2
    exact = (a/b).reshape(n,1)
    error = np.linalg.norm(Y-exact,np.inf)
    return error
   
NN = np.arange(10,100,2)
EPS = [erro(i) for i in NN]
py.semilogy(NN,EPS,'-*',markersize=8,label='$D^{1}$')
py.xlabel('$\mathbf{n}$',fontsize=27)
py.ylabel('$\mathbf{L^\infty}$ error',fontsize=27)
py.title('Log-linear',fontsize=25)
py.ylim(10**(-13),10)

# For D0.5
def Le(n):
    D = cheb(n-1,1)[0]
    Df = cheb(n+1,0.5)[0]
    A = 1
    B = 2
    C = 1
    LL = A*np.dot(D,D) + B*Df + C * np.identity(n)
    LL[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
    LL[1] = D[0]                        # boundary conditions  
    x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
    c = (np.sin(np.pi*4*x)).reshape(n,1)
    c[0] = 0
    c[1] = 0
    YY=np.linalg.solve(LL,c)
    return YY[-1]
Er = [np.abs(Le(100)-Le(i)) for i in NN]
py.semilogy(NN,Er,'g-o',markersize=8,label='$D^{0.5}$')

# For D1.5
def Lee(n):
    D = cheb(n-1,1)[0]
    Df = cheb(n+1,1.5)[0]
    A = 1
    B = 2
    C = 1
    LL = A*np.dot(D,D) + B*Df + C * np.identity(n)
    LL[0]= np.hstack(([1],np.zeros(n-1))) # boundary conditions
    LL[1] = D[0]                        # boundary conditions  
    x = np.array([-np.cos(np.pi*j/(n-1)) for j in range(n)])
    #t = (x+1)/2
    c = (np.sin(np.pi*4*x)).reshape(n,1)
    c[0] = 0
    c[1] = 0
    YY=np.linalg.solve(LL,c)
    return YY[-1]
Err = [np.abs(Lee(100)-Lee(i)) for i in NN]
py.semilogy(NN,Err,'r-^',markersize=8,label='$D^{1.5}$')
py.legend(loc=1,fontsize=17)
py.show()
