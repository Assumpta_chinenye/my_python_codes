# -*- coding: utf-8 -*-
"""
Created on Fri May  8 16:05:15 2015

@author: assumpta
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as py

NN = np.arange(0,50,1)
AC = 1/NN**2
S = np.exp(-1.5*NN**(2/3))
G = np.exp(-1.0*NN)
SG = np.exp(-NN*np.log(NN))
py.semilogy(NN,AC,'o-',label='Algebraic')
py.semilogy(NN,S,'d-',label='Subgeometric')
py.semilogy(NN,G,'*-',markersize=8,label='Geometric')
py.semilogy(NN,SG,'^-',markersize=8,label='Supergeometric')
py.ylim(10**(-10),10)
py.legend(fontsize = 13)
py.title('Log-Linear')
py.xlabel('$n$',fontsize = 15)
py.ylabel('$\log\; |a_n|$',fontsize = 17)
py.show()
