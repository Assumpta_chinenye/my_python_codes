# -*- coding: utf-8 -*-
"""
Created on Mon May  4 16:58:23 2015

@author: assumpta
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as py
from numpy import linalg as LA
from math import gamma
from numpy.linalg import inv
from my_cheb_code import cheb

py.figure('1')
'''This is for the solution plot of the true(exact) value and the aproximated value of the 
Babenko problem where f(t) = 1-sqrt(t), lambda = 2/sqrt(pi) and the exact solution is y = sqrt(t)'''

def steve(n):
    '''Takes a value n as input and computes matrix L and returns L which is n by n   '''
    l = 2/np.sqrt(np.pi)
    [D,x] = cheb(n,0.5)
    t = (x+1)/2
    F=np.eye(n-1)
    Fp=np.eye(n-1)
    f =1 - np.sqrt(t)
    fp = -1/(4*np.sqrt((x+1)/2))
    for i in range(n-1):
        F[i,i] = f[i]
        Fp[i,i] = fp[i]        
    d = cheb(n-1,1)[0]
    d = d[1:n,1:n]    
    L = 2*np.dot(F,d) + l*np.sqrt(2)*D + 2*Fp
    return L

NN  = [40,80]  
B = []
C = []
xx = []
for n in NN:
    [D,x] = cheb(n,0.5)
    t = (x+1)/2
    y = np.sqrt(t)                      # Exact solution
    C.append(y)
    xx.append(x)
    
    Fp=np.eye(n-1)
    fp = -1/(4*np.sqrt((x+1)/2))         # f prime
    for i in range(n-1):
        Fp[i,i] = fp[i]    
    c=np.diag(-2*Fp).reshape(n-1,1)
    
    L1=steve(n)                          # n by n matrix 
    A1=np.linalg.solve(L1,c)             # Approximate solution
    B.append(A1)

x1 = xx[0].reshape(NN[0]-1,1)
x2 = xx[1].reshape(NN[1]-1,1)

fig = py.figure()
ax1 = fig.add_subplot(1,2,1)
ax1.plot(x1,B[0],'b*-', label='Approximate')
ax1.plot(x1,C[0].reshape(NN[0]-1,1), 'r.-', label='Exact') 
lg = ax1.legend(loc=2, fontsize = 13)
lg.draw_frame(False)
ax1.set_xlabel('$x$',fontsize = 17)
ax1.set_ylabel('$Y(x)$', fontsize = 13)
ax1.set_title('n='+str('%s' % NN[0]))

ax2 = fig.add_subplot(1,2,2)
ax2.plot(x2,B[1],'b*-',label ='Approximate')
ax2.plot(x2,C[1].reshape(NN[1]-1,1), 'r.-',label = 'Exact') 
lg = ax2.legend(loc=2, fontsize = 13)
lg.draw_frame(False)
ax2.set_xlabel('$x$',fontsize = 17)
ax2.set_title('n='+str('%s' % NN[1]))
py.show()


py.figure('2')

def stev(n):
    ''' Takes a value n as input and computes matrix L and returns the computed error '''
    [D,x] = cheb(n,0.5)    
    l = 2/np.sqrt(np.pi)
    t = (x+1)/2
    y = np.sqrt(t).reshape(n-1,1)
    F=np.eye(n-1)
    Fp=np.eye(n-1)
    f =1 - np.sqrt(t)
    fp = -1/(4*np.sqrt((x+1)/2))
    for i in range(n-1):
        F[i,i] = f[i]
        Fp[i,i] = fp[i]
    d = cheb(n-1,1)[0]
    d = d[1:n,1:n]    
    L = 2*np.dot(F,d) + l*np.sqrt(2)*D + 2*Fp
    c=np.diag(-2*Fp).reshape(n-1,1)
    Y=np.linalg.solve(L,c)
    error = LA.norm((Y-y),np.inf)
    return error

''' Graphical plot of error for convergence '''
n1 = np.arange(10,80,2)
Ep = []
for i in n1:
    Ep.append(stev(i))
py.semilogy(n1,Ep,'o-',label='Error')
a = 1/n1**1
py.semilogy(n1,a,'*',label='$\mathbf{1/n}$')
py.xlabel('$n$',fontsize=17); py.ylabel('$L^\infty$ error',fontsize=17)
py.title('Log-Linear',fontsize=15)
py.legend()
py.show()